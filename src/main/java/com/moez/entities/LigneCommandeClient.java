package com.moez.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class LigneCommandeClient implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idLigneCommClient;

	@ManyToOne
	@JoinColumn(name = "id_article")
	private Article article;

	@ManyToOne
	@JoinColumn(name = "idCommClient")
	private CommandeClient commandeClient;

	public LigneCommandeClient() {
		// TODO Auto-generated constructor stub
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public CommandeClient getCommandeClient() {
		return commandeClient;
	}

	public void setCommandeClient(CommandeClient commandeClient) {
		this.commandeClient = commandeClient;
	}

	public Long getIdLigneCommClient() {
		return idLigneCommClient;
	}

	public void setIdLigneCommClient(Long id) {
		this.idLigneCommClient = id;
	}

}
