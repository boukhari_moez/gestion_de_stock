package com.moez.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class CommandeFournisseur implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idCommFour;
	
	@ManyToOne
	@JoinColumn(name = "idFournisseur")
	private Fournisseur fournisseur;

	@Temporal(TemporalType.TIMESTAMP)
	private Date dateCommande;
	
	@OneToMany(mappedBy = "commandeFournisseur", fetch = FetchType.LAZY)
	private List<LigneCommFournisseur> ligneCommFournisseurs;
	
	public CommandeFournisseur() {
	}
	public Long getIdCommFour() {
		return idCommFour;
	}

	public void setIdCommFour(Long id) {
		this.idCommFour = id;
	}
	public Fournisseur getFournisseur() {
		return fournisseur;
	}
	public void setFournisseur(Fournisseur fournisseur) {
		this.fournisseur = fournisseur;
	}
	public Date getDateCommande() {
		return dateCommande;
	}
	public void setDateCommande(Date dateCommande) {
		this.dateCommande = dateCommande;
	}
	public List<LigneCommFournisseur> getLigneCommFournisseurs() {
		return ligneCommFournisseurs;
	}
	public void setLigneCommFournisseurs(List<LigneCommFournisseur> ligneCommFournisseurs) {
		this.ligneCommFournisseurs = ligneCommFournisseurs;
	}

}
