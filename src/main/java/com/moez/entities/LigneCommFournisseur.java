package com.moez.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class LigneCommFournisseur implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idLigneCommFour;
    
	@ManyToOne
	@JoinColumn(name = "idCommFour")
	private CommandeFournisseur commandeFournisseur;
	
	@ManyToOne
	@JoinColumn(name = "id_article")
	private Article article;
	public LigneCommFournisseur() {
		// TODO Auto-generated constructor stub
	}

	public CommandeFournisseur getCommandeFournisseur() {
		return commandeFournisseur;
	}

	public void setCommandeFournisseur(CommandeFournisseur commandeFournisseur) {
		this.commandeFournisseur = commandeFournisseur;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public Long getIdLigneCommFour() {
		return idLigneCommFour;
	}

	public void setIdLigneCommFour(Long id) {
		this.idLigneCommFour = id;
	}

	
}
